// Modules to control application life and create native browser window
const { app, BrowserWindow, ipcMain, Menu,crashReporter } = require('electron');
const log = require('electron-log');
log.transports.file.file = './logs';
const path = require('path');
var mainWindow;


function createWindow() {
  // Menu.setApplicationMenu(null);
  // Create the browser window.
  mainWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegrationInWorker: true,
      nodeIntegration: true,
      preload: path.join(__dirname, 'preload.js')
    }
    , resizable: false
    // ,frame: false
  });


  // mainWindow.webContents.on('crashed', function () {
  //   const options = {
  //     type: 'info',
  //     title: '渲染器进程崩溃',
  //     message: '这个进程已经崩溃.',
  //     buttons: ['重载', '关闭']
  //   }
  //   dialog.showMessageBox(options, function (index) {
  //     if (index === 0) win.reload()
  //     else win.close()
  //   })

  // });


  app.on('gpu-process-crashed', function(){
 
    log.error('GPU进程崩溃，程序退出');
   
    app.exit(0);
  });

  
  mainWindow.maximize();
  // and load the index.html of the app.
  // mainWindow.loadURL('http://wxlink.icu/ac/index.html');
  mainWindow.loadFile('index.html');




  // Open the DevTools.
  // mainWindow.webContents.openDevTools()
}

ipcMain.on('menu', () => {
  Menu.setApplicationMenu(null);
})

ipcMain.on('close', () => {
  app.quit();
})

ipcMain.on('reload', () => {
  mainWindow.reload();
  // app.quit();
})

ipcMain.on("baseInfo", (event, data) => {


  const mainWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
      preload: path.join(__dirname, 'preload.js')
    }
  })

  mainWindow.maximize();
  // and load the index.html of the app.
  
  mainWindow.loadFile('baseInfo.html');
  // mainWindow.webContents.openDevTools();
  mainWindow.webContents.on('did-finish-load', function () {
    mainWindow.webContents.send('dataJsonPort', data);
  });
})
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  mainWindow.webContents.openDevTools();

  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})




// try {
//   require('electron-reloader')(module,{});
// } catch (_) {}


// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
