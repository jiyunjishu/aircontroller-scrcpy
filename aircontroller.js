images.requestScreenCapture();
// setInterval(()=>{
//     images.save(images.captureScreen(),'/sdcard/screen.png',"png", 10);
// },200);


importClass('java.io.BufferedReader');
importClass('java.io.BufferedWriter');
importClass('java.io.IOException');
importClass('java.io.InputStream');
importClass('java.io.InputStreamReader');
importClass('java.io.OutputStream');
importClass('java.io.PrintWriter');
importClass('java.io.DataOutputStream');
importClass('java.net.Socket');
importClass('java.net.ServerSocket');
importClass('java.io.DataInputStream');
importClass('java.io.OutputStreamWriter');

var serversocket = new ServerSocket(9999);
log('服务端已经启动,正在等待客户端连接...');

while(true){
    var socket = serversocket.accept();
    // log("远程主机地址：" + serversocket.getRemoteSocketAddress());
    // log("server发送缓冲区大小"+serversocket.getSendBufferSize());
    log("server接收缓冲区大小"+socket.getReceiveBufferSize());
    log("发送缓冲区大小"+socket.getSendBufferSize());
    log("接收缓冲区大小"+socket.getReceiveBufferSize());
    // socket.setSendBufferSize(1);
    // socket.setReceiveBufferSize(500);
    log("接收缓冲区大小"+socket.getSendBufferSize());
    socket.setSendBufferSize(10000);
    var inputStream = socket.getInputStream();
    var inputStreamReader = new InputStreamReader(inputStream);
    var bufferedReader = new BufferedReader(inputStreamReader);
    var temp = null;
    var info = "";
    temp = bufferedReader.readLine();
    if(temp != null) {
        info += temp;
        log("已接收到客户端连接\n");
        log("收到客户端信息：\n"+info+"\n\n当前客户端ip为：\n"+socket.getInetAddress().getHostAddress());
        // break;
    }
    var dis = new DataInputStream(socket.getInputStream());
    var out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(),"UTF-8"),500);
    // var out = new DataOutputStream(socket.getOutputStream());
    // ars = StrCut2Arr(images.toBase64(images.captureScreen(), "png", 10),1000);
    // for(var i=0;i<ars.length;i++){
    //     out.write(ars[i]);
    // }
    
    threads.start(function () {
        while (true) {
            // var out = socket.getOutputStream().write("123");
            // out.flush();
            var out1 = new OutputStreamWriter(socket.getOutputStream(),"UTF-8");
            var img = images.toBase64(images.captureScreen(), "jpeg", 10);
            out1.write(img.length+'');
            out1.write(-1);//输出终结符
            out1.flush();
            out1.write(img,0,img.length);
            out1.flush();
            sleep(20);
        }
        // var out1 = new OutputStreamWriter(socket.getOutputStream(),"UTF-8");
        // var img = images.toBase64(images.captureScreen(), "png", 10);
        // out1.write(img.length+'');
        // out1.flush();
        // out.write(img);
        // out.flush();
        // sleep(10000);
    });
    
    // out.write(images.toBase64(images.captureScreen(), "png", 10));
    // out.write('123');
    // out.flush();

    // setTimeout(function(){
    //     log(123);
    //     out.write('444');
    // },10000);

}




var outputStream = socket.getOutputStream();
var printWriter = new PrintWriter(outputStream);
printWriter.print("你好，服务端已接收到您的信息");
printWriter.flush();
socket.shutdownOutput();//关闭输出流
//关闭对应资源
printWriter.close();
outputStream.close();
bufferedReader.close();
inputStream.close();
serversocket.close();

function StrCut2Arr(str,n){
	var arr=[];
	var len=Math.ceil(str.length/n);
	for(var i=0;i < len;i++){
		if(str.length >= n){
			var strCut=str.substring(0,n);
			arr.push(strCut);
			str=str.substring(n);
		}else{
			str=str;
			arr.push(str);
		}
	}
	return arr;
}