var cmd = require('node-cmd');
const path = require('path');
const adb_path = path.join(__dirname, '../adb/adb.exe');
var lastData = [];
loadDevices();
setInterval(loadDevices, 10000);


function loadDevices(){
    let stateMap = {
        device:'已连接电脑',
        offline:'无法链接',
        unauthorized:'未授权'
    };

    let data2 = [];

    cmd.get(adb_path + " devices", (e, d, s) => {
        res = d.split("\n");
        res.forEach((item, index) => {
            if (index < 1 || !(item.replace(/[ ]*/g, '')) || item.length == 1) {
                return
            }

            var deviceInfo = item.replace("\r","").split("	");

            data2.push({ client: deviceInfo[0],stateName:stateMap[deviceInfo[1]] });
        });

        // if(JSON.stringify(lastData)!=JSON.stringify(data2)){
        //     lastData = data2;
        //     postMessage(data2);
        // }    
        postMessage(data2);
  
        
    });
}