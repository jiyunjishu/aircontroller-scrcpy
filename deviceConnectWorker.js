var net = require("net");
try {

    var device;
    var count =1;
    self.onmessage = e => {
        device = e.data;
        var _client = net.connect(
            {
                port: device.port
            }, function (err) {
                if (err) {
                    console.log('connect socket server error');
                    return;
                }

                var readBannerBytes = 0;
                var bannerLength = 2;
                var readFrameBytes = 0;
                var frameBodyLength = 0;
                var imageBodyIndex = 0;
                var frameBody = Buffer.alloc(1024*1024);
                var lastImgURLObj;
                var banner = {
                    version: 0
                    , length: 0
                    , pid: 0
                    , realWidth: 0
                    , realHeight: 0
                    , virtualWidth: 0
                    , virtualHeight: 0
                    , orientation: 0
                    , quirks: 0
                };


                function tryRead() {
                    for (var chunk; (chunk = _client.read());) {
                        // console.info('chunk(length=%d)', chunk.length)
                        for (var cursor = 0, len = chunk.length; cursor < len;) {
                            if (readBannerBytes < bannerLength) {
                                switch (readBannerBytes) {
                                    case 0:
                                        // version
                                        banner.version = chunk[cursor];
                                        break;
                                    case 1:
                                        // length
                                        banner.length = bannerLength = chunk[cursor];
                                        break;
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 5:
                                        // pid
                                        banner.pid +=
                                            (chunk[cursor] << ((readBannerBytes - 2) * 8)) >>> 0;
                                        break;
                                    case 6:
                                    case 7:
                                    case 8:
                                    case 9:
                                        // real width
                                        banner.realWidth +=
                                            (chunk[cursor] << ((readBannerBytes - 6) * 8)) >>> 0;
                                        break;
                                    case 10:
                                    case 11:
                                    case 12:
                                    case 13:
                                        // real height
                                        banner.realHeight +=
                                            (chunk[cursor] << ((readBannerBytes - 10) * 8)) >>> 0;
                                        break;
                                    case 14:
                                    case 15:
                                    case 16:
                                    case 17:
                                        // virtual width
                                        banner.virtualWidth +=
                                            (chunk[cursor] << ((readBannerBytes - 14) * 8)) >>> 0;
                                        break;
                                    case 18:
                                    case 19:
                                    case 20:
                                    case 21:
                                        // virtual height
                                        banner.virtualHeight +=
                                            (chunk[cursor] << ((readBannerBytes - 18) * 8)) >>> 0;
                                        break;
                                    case 22:
                                        // orientation
                                        banner.orientation += chunk[cursor] * 90;
                                        break;
                                    case 23:
                                        // quirks
                                        banner.quirks = chunk[cursor];
                                        break;
                                }

                                cursor += 1;
                                readBannerBytes += 1;

                                if (readBannerBytes === bannerLength) {
                                    console.log('banner', banner);
                                }
                            }
                            else if (readFrameBytes < 4) {
                                frameBodyLength += (chunk[cursor] << (readFrameBytes * 8)) >>> 0;
                                cursor += 1;
                                readFrameBytes += 1;
                                // console.info('headerbyte%d(val=%d)', readFrameBytes, frameBodyLength)
                            }
                            else {
                                if (len - cursor >= frameBodyLength) {
                                    // console.info('bodyfin(len=%d,cursor=%d)', frameBodyLength, cursor)  
                                    // frameBody = Buffer.concat([
                                    //     frameBody
                                    //     , chunk.slice(cursor, cursor + frameBodyLength)
                                    // ])

                                    for(var i=cursor;i<cursor+frameBodyLength;i++){
                                        frameBody.writeUInt8(chunk[i],imageBodyIndex);
                                        imageBodyIndex++;
                                    }

                                    // // Sanity check for JPG header, only here for debugging purposes.
                                    // if (frameBody[0] !== 0xFF || frameBody[1] !== 0xD8) {
                                    //     console.error(
                                    //         'Frame body does not start with JPG header', frameBody)
                                    //     process.exit(1)
                                    // }

                                    // ws.send(frameBody, {
                                    //   binary: true
                                    // })
                                    // loadImg(frameBody, deviceId);
                                    // let tempBuf = frameBody.subarray(0,imageBodyIndex);
                                    // let data = {buf:frameBody.subarray(0,imageBodyIndex),device:device.id};
                                    // postMessage({buf:frameBody.subarray(0,imageBodyIndex),device:device.id});
                                    // tempBuf = null;
                                    // var ab = new ArrayBuffer(1);

                                    var ab = new ArrayBuffer(imageBodyIndex);
                                    var view = new Uint8Array(ab);
                                    for (var i = 0; i < imageBodyIndex; ++i) {
                                        view[i] = frameBody[i];
                                    }
                                    let data = {buf:ab,device:device.id};
                                    postMessage(data, [data.buf]);
                                    view = null;
                                    ab = null;
                                    data = null;
                                    cursor += frameBodyLength;
                                    // frameBody = null;
                                    frameBodyLength = readFrameBytes =imageBodyIndex = 0;
                                    // frameBody = new Buffer(0)
                                }
                                else {
                                    // console.info('body(len=%d)', len - cursor)

                                    // frameBody = Buffer.concat([
                                    //     frameBody
                                    //     , chunk.slice(cursor, len)
                                    // ]);

                                    for(var i=cursor;i<len;i++){
                                        frameBody.writeUInt8(chunk[i],imageBodyIndex);
                                        imageBodyIndex++;
                                    }

                                    frameBodyLength -= len - cursor;
                                    readFrameBytes += len - cursor;
                                    cursor = len;
                                }
                            }
                        }
                    }
                }


                _client.on('readable', tryRead);


                _client.on('timeout', function (err) {
                    console.error("连接超时:重试");
                });

                _client.on('connect', function (err) {
                    console.log("连接设备成功");
                    app.connectMap[deviceId] = _client;
                });
            });
            
        _client.on('error', function (err) {
            console.error("连接失败:重试");
        });

    };


} catch (error) {
    console.error(error);
    console.error("连接失败:重试");
}