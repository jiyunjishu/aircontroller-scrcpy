function connectDevice(deviceId) {

    if (app.connectMap[deviceId]) {
        try {
            app.connectMap[deviceId].end();
        } catch (error) {

        }
    }
    var buf = '';
    var contentLength = 0;
    app.devices[deviceId].source = "123";
    try {


        var _client = net.connect(
            {
                port: app.devices[deviceId].port, host: '127.0.0.1'
            }, function (err) {

                app.connectMap[deviceId] = _client;
                if (err) {
                    console.log('connect socket server error');
                    app.connectMap[deviceId] = null;
                    return;
                }
                _client.on('data', function (msg) {
                    // _client.end()
                    if (msg.length > 10) {
                        buf += msg;
                        if (buf.length == contentLength) {
                            if (app.screensMap[deviceId] == null) {
                                return;
                            }
                            try {
                                app.screens[app.screensMap[deviceId]].source = buf;
                            } catch (error) {
                                console.debug(app.screens[app.screensMap[deviceId]])
                            }
                            app.screens[app.screensMap[deviceId]].source = buf;
                            buf = null;
                        }
                    }
                    else {
                        buf = 'data:image/jpeg;base64,'
                        contentLength = parseInt(msg) + 23;
                    }

                })
                _client.on('error', function (err) {
                    console.error("连接失败:重试");
                    setTimeout(() => {
                        if (app.devices[deviceId]) {
                            connectDevice(deviceId);
                        } else {
                            app.connectMap[deviceId] = null;
                        }
                    }, 10000);
                })

                _client.on('timeout', function (err) {
                    console.error("连接超时:重试");
                    setTimeout(() => {
                        if (app.devices[deviceId]) {
                            connectDevice(deviceId);
                        } else {
                            app.connectMap[deviceId] = null;
                        }
                    }, 10000);
                })

                _client.on('connect', function (err) {
                    console.log("连接设备成功");
                    app.connectMap[deviceId] = _client;
                })
            })

    } catch (error) {
        console.error(error);
        console.error("连接失败:重试");
        setTimeout(() => {
            if (app.devices[deviceId]) {
                connectDevice(deviceId);
            } else {
                app.connectMap[deviceId] = null;
            }
        }, 10000);
    }
}
