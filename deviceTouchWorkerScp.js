var net = require("net");

var device;
var _touch;
var lastPosint = [];
self.onmessage = e => {


    if (e.data.event == 'connect') {
        device = e.data.data;
        _touch = net.connect({
            port: device.port
        }, err => {
            console.debug(err);
        })
        _touch.on('error', (e) => {
            console.error("控制模块连接失败" + e);
        })
    } else if (e.data.event == 'update') {
        device = e.data.data;
    } else {

        var command = e.data;
        let buf;
        switch (command[0]) {
            case "d":
                buf = Buffer.allocUnsafe(28);
                buf.writeInt8(2, 0);
                buf.writeInt8(0, 1);
                buf.writeUInt32BE(command[1], 2);
                buf.writeUInt32BE(1, 6);
                lastPosint[0] = command[2];
                lastPosint[1] = command[3];
                buf.writeUInt32BE(lastPosint[0], 10);
                buf.writeUInt32BE(lastPosint[1], 14);
                //坐标
                buf.writeUInt16BE(device.wm[0], 18);
                buf.writeUInt16BE(device.wm[1], 20);
                buf.writeUInt16BE(50, 22);
                buf.writeUInt32BE(0, 24);
                _touch.write(buf);
                break;
            case "u":
                buf = Buffer.allocUnsafe(28);
                buf.writeInt8(2, 0);
                buf.writeInt8(1, 1);
                buf.writeUInt32BE(command[1], 2);
                buf.writeUInt32BE(1, 6);
                buf.writeUInt32BE(parseInt(lastPosint[0]), 10);
                buf.writeUInt32BE(parseInt(lastPosint[1]), 14);
                //坐标
                buf.writeUInt16BE(device.wm[0], 18);
                buf.writeUInt16BE(device.wm[1], 20);
                //压力
                buf.writeUInt16BE(50, 22);
                buf.writeUInt32BE(0, 24);
                _touch.write(buf);
                break;
            case "m":
                buf = Buffer.allocUnsafe(28);
                buf.writeInt8(2, 0);
                buf.writeInt8(2, 1);
                buf.writeUInt32BE(command[1], 2);
                buf.writeUInt32BE(1, 6);
                lastPosint[0] = command[2];
                lastPosint[1] = command[3];
                buf.writeUInt32BE(lastPosint[0], 10);
                buf.writeUInt32BE(lastPosint[1], 14);
                buf.writeUInt16BE(device.wm[0], 18);
                buf.writeUInt16BE(device.wm[1], 20);
                buf.writeUInt16BE(50, 22);
                buf.writeUInt32BE(0, 24);
                _touch.write(buf);
                break;
            case "f":
                buf = Buffer.allocUnsafe(5);
                buf.writeInt8(11, 0);
                buf.writeUInt32BE(command[1], 1);
                _touch.write(buf);
                break;
            case "s":
                buf = Buffer.allocUnsafe(5);
                buf.writeInt8(12, 0);
                buf.writeUInt32BE(command[1], 1);
                _touch.write(buf);
                break;
            case "q":
                buf = Buffer.allocUnsafe(5);
                buf.writeInt8(13, 0);
                buf.writeUInt32BE(command[1], 1);
                _touch.write(buf);
                break;
            case "k":
                buf = Buffer.allocUnsafe(10);
                buf.writeInt8(0, 0);
                buf.writeInt8(0, 1);
                buf.writeUInt32BE(command[1], 2);
                buf.writeUInt32BE(0, 6);
                _touch.write(buf);

                buf = Buffer.allocUnsafe(10);
                buf.writeInt8(0, 0);
                buf.writeInt8(1, 1);
                buf.writeUInt32BE(command[1], 2);
                buf.writeUInt32BE(0, 6);
                _touch.write(buf);
                break;
            case "l":
                buf = Buffer.allocUnsafe(1);
                buf.writeInt8(14, 0);
                _touch.write(buf);        
                break;
            case "ul": //返回，解锁
                buf = Buffer.allocUnsafe(1);
                buf.writeInt8(4, 0);
                _touch.write(buf);
                break;
            case "cp": //复制
                let msg = Buffer.from(command[1],"utf-8");
                let ev = Buffer.allocUnsafe(3);
                ev.writeInt8(8, 0);
                ev.writeUInt16BE(msg.length, 1);
                ev = Buffer.concat([
                    ev
                    , msg
                ]);
                _touch.write(ev);
                break;
            default:
                break;
        }

    }

};
