

exports.connectDeviceCommon = function (app, deviceId) {

    if (app.connectMap[deviceId]) {
        try {
            app.connectMap[deviceId + "_tc"].terminate();
            app.connectMap[deviceId].end();
        } catch (error) {
            console.error(error);
        }
    }


    try {


        var _client = net.connect(
            {
                port: app.devices[deviceId].port
            }, function (err) {

                app.connectMap[deviceId] = _client;
                if (err) {
                    console.log('connect socket server error');
                    app.connectMap[deviceId] = null;
                    return;
                }
                var count = 1;
                var readBannerBytes = 0;
                var bannerLength = 2;
                var readFrameBytes = 0;
                var frameBodyLength = 0;
                var frameBody = new Buffer(0);
                var tempFrame;
                var tempTimer;
                var banner = {
                    version: 0
                    , length: 0
                    , pid: 0
                    , realWidth: 0
                    , realHeight: 0
                    , virtualWidth: 0
                    , virtualHeight: 0
                    , orientation: 0
                    , quirks: 0
                };


                function tryRead() {
                    for (var chunk; (chunk = _client.read());) {
                        // console.info('chunk(length=%d)', chunk.length)
                        for (var cursor = 0, len = chunk.length; cursor < len;) {
                            if (readBannerBytes < bannerLength) {
                                switch (readBannerBytes) {
                                    case 0:
                                        // version
                                        banner.version = chunk[cursor]
                                        break;
                                    case 1:
                                        // length
                                        banner.length = bannerLength = chunk[cursor]
                                        break;
                                    case 2:
                                    case 3:
                                    case 4:
                                    case 5:
                                        // pid
                                        banner.pid +=
                                            (chunk[cursor] << ((readBannerBytes - 2) * 8)) >>> 0
                                        break;
                                    case 6:
                                    case 7:
                                    case 8:
                                    case 9:
                                        // real width
                                        banner.realWidth +=
                                            (chunk[cursor] << ((readBannerBytes - 6) * 8)) >>> 0
                                        break;
                                    case 10:
                                    case 11:
                                    case 12:
                                    case 13:
                                        // real height
                                        banner.realHeight +=
                                            (chunk[cursor] << ((readBannerBytes - 10) * 8)) >>> 0
                                        break;
                                    case 14:
                                    case 15:
                                    case 16:
                                    case 17:
                                        // virtual width
                                        banner.virtualWidth +=
                                            (chunk[cursor] << ((readBannerBytes - 14) * 8)) >>> 0
                                        break;
                                    case 18:
                                    case 19:
                                    case 20:
                                    case 21:
                                        // virtual height
                                        banner.virtualHeight +=
                                            (chunk[cursor] << ((readBannerBytes - 18) * 8)) >>> 0
                                        break;
                                    case 22:
                                        // orientation
                                        banner.orientation += chunk[cursor] * 90
                                        break;
                                    case 23:
                                        // quirks
                                        banner.quirks = chunk[cursor];
                                        break;
                                }

                                cursor += 1;
                                readBannerBytes += 1;

                                if (readBannerBytes === bannerLength) {
                                    if (!app.renameMap[deviceId]) {
                                        app.renameMap[deviceId] = deviceId;
                                    }

                                    var item = document.getElementById(deviceId + "_check");
                                    if (app.accessToken.clientNum <= app.linkDeviceCount && item == null) {
                                        _client.end();
                                        return;
                                    }

                                    if (item == null) {
                                        app.linkDeviceCount++;
                                        app.screens.push({ name: deviceId, source: '', wm: app.devices[deviceId].wm });
                                        app.screensMap[deviceId] = app.screens.length - 1;
                                    }
                                }
                            }
                            else if (readFrameBytes < 4) {
                                frameBodyLength += (chunk[cursor] << (readFrameBytes * 8)) >>> 0;
                                cursor += 1;
                                readFrameBytes += 1;
                                // console.info('headerbyte%d(val=%d)', readFrameBytes, frameBodyLength)
                            }
                            else {
                                if (len - cursor >= frameBodyLength) {
                                    // console.info('bodyfin(len=%d,cursor=%d)', frameBodyLength, cursor)

                                    frameBody = Buffer.concat([
                                        frameBody
                                        , chunk.slice(cursor, cursor + frameBodyLength)
                                    ])

                                    // Sanity check for JPG header, only here for debugging purposes.
                                    if (frameBody[0] !== 0xFF || frameBody[1] !== 0xD8) {
                                        console.error(
                                            'Frame body does not start with JPG header', frameBody);
                                        process.exit(1);
                                    }

                                    // ws.send(frameBody, {
                                    //   binary: true
                                    // })
                                    var canvas = document.getElementById(deviceId + "_t");

                                    if (canvas) {
                                        loadMainImg(frameBody, canvas);
                                    } else {
                                        let rd = Math.random() * 10;
                                        tempFrame = null;
                                        tempFrame = frameBody;
                                        try {
                                            clearTimeout(tempTimer);
                                        } catch (error) { }
                                        if (rd <= app.frameNum) {
                                            canvas = document.getElementById(deviceId);
                                            loadImg(frameBody, canvas);
                                        } else {
                                            tempTimer = setTimeout(() => {
                                                let canvas = document.getElementById(deviceId);
                                                loadImg(tempFrame, canvas);
                                            }, 150);
                                        }
                                    }
                                    frameBody = null;
                                    cursor += frameBodyLength;
                                    frameBodyLength = readFrameBytes = 0;
                                    frameBody = new Buffer(0);
                                }
                                else {
                                    // console.info('body(len=%d)', len - cursor)

                                    frameBody = Buffer.concat([
                                        frameBody
                                        , chunk.slice(cursor, len)
                                    ]);

                                    frameBodyLength -= len - cursor;
                                    readFrameBytes += len - cursor;
                                    cursor = len;
                                }
                            }
                        }
                    }
                }



                _client.on('readable', tryRead)


                _client.on('timeout', function (err) {
                    console.error("连接超时:重试");
                    // setTimeout(() => {
                    //     if (app.devices[deviceId]) {
                    //         connectDevice(deviceId);
                    //     } else {
                    //         app.connectMap[deviceId] = null;
                    //     }
                    // }, 10000);
                })

                _client.on('connect', function (err) {
                    console.log("连接设备成功");
                    app.connectMap[deviceId] = _client;
                })
            });

        _client.on('error', function (err) {
            console.error("连接失败:重试");
        })

    } catch (error) {
        console.error(error);
        console.error("连接失败:重试");
    }

    app.connectMap[deviceId + "_tc"] = new Worker('deviceTouchWorker.js');
    app.connectMap[deviceId + "_tc"].postMessage({ event: 'connect', data: app.devices[deviceId] });


}


exports.connectDeviceByThread = function (app, deviceId) {

    if (app.connectMap[deviceId]) {
        try {
            app.connectMap[deviceId].terminate();
            app.connectMap[deviceId + "_tc"].terminate();
        } catch (error) {
            console.error(error);
        }
    }
    var count = 1;
    var tempFrame;
    var tempTimer;
    app.connectMap[deviceId] = new Worker('deviceConnectWorker.js');
    app.connectMap[deviceId].postMessage(app.devices[deviceId]);
    app.connectMap[deviceId].onmessage = (event) => {

        try {
            var canvas = document.getElementById(event.data.device + "_t");
            if (canvas) {
                loadMainImg(event.data.buf, canvas);
            } else {
                let rd = Math.random() * 10;
                // if (rd <= app.frameNum) {
                //     canvas = document.getElementById(event.data.device);
                //     loadImg(event.data.buf, canvas);
                //     count++;
                // }

                tempFrame = null;
                tempFrame = event.data.buf;
                try {
                    clearTimeout(tempTimer);
                } catch (error) { }

                if (rd <= app.frameNum) {
                    canvas = document.getElementById(deviceId);
                    loadImg(event.data.buf, canvas);
                } else {
                    tempTimer = setTimeout(() => {
                        let canvas = document.getElementById(deviceId);
                        console.debug(1);
                        loadImg(tempFrame, canvas);
                    }, 150);
                }
            }


            delete event.data;
            event = null;

        } catch (error) {
            console.error(error);
        }

    };

    app.connectMap[deviceId + "_tc"] = new Worker('deviceTouchWorker.js');
    app.connectMap[deviceId + "_tc"].postMessage({ event: 'connect', data: app.devices[deviceId] });

}
