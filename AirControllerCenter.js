var WebSocketServer = require('ws').Server,
    wss = new WebSocketServer({ port: 7799 });//服务端口8181
var _client, client1;
wss.on('connection', function (ws) {
    console.log('服务端：客户端已连接');
    if (_client) {
        client1 = ws;
        console.log('服务端：控制器连接');
        client1.on('message', function (message) {
            self.postMessage(message);
        });
    } else {
        console.log('服务端：画面传输连接');
        _client = ws;
    }

});

self.onmessage = (e) => {

    if (e.data == 'disconnect') {
        try {
            _client.end();
        } catch (error) {

        }

        try {
            client1.end();
        } catch (error) {
            
        }
        _client = null;
        client1 = null;
    }
    if (_client) {
        _client.send(e.data);
    }

    e = null;
}


function toArrayBuffer(buf) {
    var ab = new ArrayBuffer(buf.length);
    var view = new Uint8Array(ab);
    for (var i = 0; i < buf.length; ++i) {
        view[i] = buf[i];
    }
    return ab;
}